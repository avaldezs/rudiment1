const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');

const app = express();
var config = {
	host: 'localhost',
	user: 'armando',
	password: 'Chomusuke',
	database: 'signup'
};

var pool = new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api', async (req, res) => {
	var workshop = req.query.workshop;
	var search = await pool.query('select * from workshops where workshop = $1', [workshop]);
	if(!workshop){
		var response = await pool.query('select * from workshops');
		let results = response.rows.map(function(workshops){
			return workshops.workshop;
		});
		res.json({workshops: results});
	}
	else if(search.rows.length == 0){
		res.json({error: 'Workshop not found.'});
	}

	else{
		try{
			var response = await pool.query('select attendee from attendance where workshop = $1', [workshop]);
			console.log(JSON.stringify(response.rows));
			let results = response.rows.map(function(attendance){
				return attendance.attendee;
			});
			res.json({attendees: results});
		}
		catch(e) {
			console.error('Error running query ' + e);
		}
	}
});

app.post('/api', async (req, res) =>{
	console.log(req.body);
	var workshop = req.body.workshop;
	var attendee = req.body.attendee;
	var response = await pool.query('select * from attendance where workshop = $1 and attendee = $2', [workshop, attendee]);
	var workshopsearch = await pool.query('select * from workshops where workshop = $1', [workshop]);
	if((!attendee) || (!workshop)){
		res.json({error: 'parameters not given'});
	}
	else if(response.rows.length == 1){
		res.json({error: 'Attendee already enrolled.'});
		console.log('attendee already enrolled');
	}
	else if(workshopsearch.rows.length == 0){
		//res.json('Error: Workshop not found');
		//console.log('Error: Workshop not found');
		try{

			var newWorkshop = await pool.query('insert into workshops values ($1)', [workshop]);
			var insertion = await pool.query('insert into attendance values ($1, $2)', [workshop, attendee]);
			res.json('Attendee enrolled');
		}
		catch(e){
			console.log('error creating workshop', e);
		}
	}
	else{
		try{
			var insertion = await pool.query('insert into attendance values ($1, $2)', [workshop, attendee]);
			var response = await pool.query('select * from attendance where workshop = $1 and attendee = $2', [workshop, attendee]);
			res.json({attendee: attendee, workshop: workshop});
			//  res.json(response.rows);
		}
		catch(e){
			console.log('Error running insert', e);
		}
	}
});

app.delete('/api', async (req, res) =>{
	console.log(req.body);
	var workshop = req.body.workshop;
	var attendee = req.body.attendee;
	if(!workshop && !attendee){
		res.json({error: 'parameters not given'});
	}
	else if(!attendee){
		var deletion = await pool.query('delete from attendance where attendee is null');
		res.json('non-existant attendee removed');
	}
	else{
		try{
			var response = await pool.query('delete from attendance where workshop = $1 and attendee = $2', [workshop, attendee]);
			res.json({status: 'deleted'});
		}
		catch(e){
			console.log('Error running delete', e);
		}
	}
});
app.listen(app.get('port'), () => {
	console.log('Running');
});
