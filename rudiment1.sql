create database signup;
\c signup
create table attendance (
    workshop text,
    attendee text
);

insert into attendance values ('React Fundamentals', 'Armando Valdez'),
    ('React Fundamentals', 'Ahmed Abdelali'),
    ('React Fundamentals', 'Ann Frank'),
    ('React Fundamentals', 'Ann Mulkern'),
    ('React Fundamentals', 'Clara Weick'),
    ('React Fundamentals', 'James Archer'),
    ('React Fundamentals', 'Linda Park'),
    ('React Fundamentals', 'Lucy Smith'),
    ('React Fundamentals', 'Roz Billingsley'),
    ('React Fundamentals', 'Samantha Eggert'),
    ('React Fundamentals', 'Tim Smith');
    
create table workshops (
    workshop text
    );

insert into workshops values ('DevOps 101'),
    ('Docker Container Fundamentals'),
    ('Machine Learning'),
    ('Modern Javascript'),
    ('MongoDB'),
    ('React Fundamentals'),
    ('Self-Driving Cars');
